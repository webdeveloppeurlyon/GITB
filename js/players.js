//Classe pour la création d'instances des joueurs du club
class Player{
	constructor(firstName, lastName, mail, tel, optionTournois){
		this.firstName = firstName;
		this.lastName = lastName;
		this.mail = mail;
		this.tel = tel;
		this.optionTournois = optionTournois;
	}
};

//Tableau des joueurs du club
let list_player = [];

//Fonction pour créer une instance d'un joueur et l'ajouter à la liste du club
const ajout_player = (licence, prenom, nom, mail, tel, optionTournois) =>{
	licence = new Player(prenom, nom, mail , tel, optionTournois);
	list_player.push(licence);
	return licence;
};

//Test pour la suite
//ajout_player(343,"Fanny","Fort","fff@f.fr", 2453424, true);

let test = document.getElementById("test_option");
function maj(){
	test.innerHTML = list_player[0].firstName;
};

/*fonction delete child
function deleteChild() { 
	var e = document.getElementById("select_player"); 

        //e.firstElementChild can be used. 
        var child = e.lastElementChild;  
        while (child) { 
        	e.removeChild(child); 
        	child = e.lastElementChild; 
        } 
    }*/ 

//Fonction pour créer le menu déroulant
function createListe(){
	// Variable pour simplifier la fonction
	let parent = document.getElementById("select_player"); 
	let child = parent.lastElementChild;  
	//Permet de réinitialiser la liste des joueurs pour ne pas avoir de doublons
	while (child){ 
		parent.removeChild(child); 
		child = parent.lastElementChild; 
	};
	parent.appendChild(document.createElement("option")).innerHTML = "Sélectionne un joueur"; //Création de la ligne par défaut de la liste
	for(let player of list_player){
    	parent.appendChild(document.createElement("option")).innerHTML = `${player.firstName} ${player.lastName}`; //création d'un nouvelle balise option
    };
};
//A voir plus tard

let input = document.getElementById("select_player");
let output = document.getElementById("nom");

input.addEventListener("input", function(event){
	output.innerHTML = event.target.value;
})
