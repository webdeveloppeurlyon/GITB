const tableJoueurs = "Joueurs/"
const tableTournois = "Tournois/"
const tableContact = "Contact/"

let APIURL = "https://api.airtable.com/v0/appUZdvox0Zvhc3ZD/";
const KeyAPI = "?api_key=keygHycxBbIn4H0c6";
let id = "";

//Liste des clés utilisées dans airtable
	//Clés du tableau tournoi
	let cleTournoi = ["Nom_du_tournoi", "Date", "A_ajouter_site", "Date_limite_inscription", "Envoi 1", "Envoi 2", "Envoi 3", "Description"];

	let api_demande_t = () =>{
		return new Promise((resolve) =>{
			let request = new XMLHttpRequest();
			request.onreadystatechange = function() {
				if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
					let response = JSON.parse(this.responseText);
					resolve(response);
					console.log("connection ok")
				}
			};
			request.open("GET", APIURL + tableTournois + id +KeyAPI);
			request.send();
		});
	}
	async function test(){
		const list = await api_demande_t();
		console.log(Object.keys(list.records[1].fields))
	};

	async function creatListTournoi(){
		try{
			const list = await api_demande_t();
		//Création de la ligne du tableau pour chaque tournoi
		list.records.forEach((tournoi) =>{
			let tableau = document.getElementById("tournoisArrayListe");
			//Création de la ligne du tournoi
			let ligne = document.createElement("tr")
			tableau.appendChild(ligne);
			//Contenu de chaque colonne
			for (let i = 0; i < 8; i++) {
				let elt = document.createElement("td");
				elt.setAttribute("class", "r " + cleTournoi[i])
				ligne.appendChild(elt).innerHTML = tournoi.fields[cleTournoi[i]];
			};
			//A TRAITER
			let linkColonne = document.createElement("td");
			linkColonne.setAttribute("class", "");
			let link = document.createElement("a");
			link.setAttribute("href", "id_detail.html?id=" + tournoi.id);
			ligne.appendChild(linkColonne);
			linkColonne.appendChild(link).innerHTML = '<i class="far fa-eye eye_icon"></i>';
		});}
		catch{
			console.log('ERROR')
		}
	};

	async function detailId(){
		    //Collecter l'URL après le ?id= pour le récupérer uniquement sur l'API
		    id = location.search.substring(4);
		    const list = await api_demande_t();
		    console.log("fiche id "+id);
		    console.log(list)
		};

		/*COLONNE DE GAUCHE
			titre du tournoi
			date
			Adresse
			description
			contact
			ordre cheque
			Générateur code HTML
		COLLONE DROITE
			liste des joueurs inscrits*/
