# GITB
# Author : Nicolas Lachise pour Web Dev Lyon
# Mail : webdeveloppeurlyon@gmail.com

Application (web) de gestion des joueurs en tournoi de badminton

Dans la gestion des clubs de badminton un certains nombres de tournois sont payés aux adhérents. Ce projet est un dashboard permettant le suivi des inscriptions des joueurs aux différents tournois de badminton.

Le cahier des charges initial demande :

_ Le dashboard doit contenir la liste des joueurs ayant adhéré au club et à l'option "tournois" => pour le moment cette liste est au format excel et ne sera pas partagé pour des raisons de confidentialité.
_ Pour chaque joueur on doit avoir comme information : les tournois inscrits, les coordonnées, la possibilité de le contacter par mail (mailto)
_ Les accès vers les page des sites organisateur de tournois (essentiellement Badiste et Badnet), le site et Facebook du club utilisateur.

v.0.0.0 :
Initialisation du projet
Design du projet en HTML/CSS
Navbar positionnée à gauche pour sélection des joueurs
Bandeau en haut avec nom de l'application et version
